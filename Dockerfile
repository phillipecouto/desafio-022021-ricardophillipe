FROM maven:3.5.2-jdk-8-alpine AS MAVEN_TOOL_CHAIN
COPY pom.xml /tmp/
COPY src /tmp/src/
WORKDIR /tmp/
RUN mvn package

ENTRYPOINT ["java", "-jar", "-Dconfig=/tmp/arquivos_shopping/config.properties", "/tmp/target/desafio-022021-RicardoPhillipe.jar"]