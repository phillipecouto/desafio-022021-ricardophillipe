# Desafio Shopping Samsung, CIn-UFPE e Fade.

`Ricardo Phillipe Couto Araujo Junior`

## **Sobre o Projeto**

O projeto foi criado com a estrutura MAVEN e pode ser aberto em qualquer IDE Java,
como [Intelijj IDEA](https://www.jetbrains.com/pt-br/idea/) (escolhida para elaboração deste projeto) ou 
[Eclipse](https://www.eclipse.org/downloads/) entre outras. 

A única dependência que há no projeto é a JUnit 4.13.2, utilizada para realizar
testes da aplicação.

Como a aplicação é feita em Java faz necessário ter o [Java 8](https://www.oracle.com/br/java/technologies/javase/javase-jdk8-downloads.html) e o [Maven](https://maven.apache.org/download.cgi) instalados para 
abrir, compilar e rodar a aplicação na máquina local. Mais adiante, existe uma sessão explicando
os passos para rodar a aplicação por [Docker](https://www.docker.com/products/docker-desktop) e máquina local.


Para estruturar o projeto visando escalabilidade, baixa complexidade de entendimento e
performance, foi utilizado um padrão MVC com multicamadas. A seguir uma breve explicação
de cada camada:

**Camada de entidades  Entity**

Foi adotado o padrão Builder na construção dos objetos das entidades
para facilidar o desenvolvedor a escrita/leitura do código.    

![picture](readme%20img/Package%20entity.png)
    

    


**Camada de persistencia DAO**

Foi padronizada por Interfaces, visando a escalabilidade
e a fácil troca de base de persistência.

Na implementação atual do projeto, a camada de dados é feita por arquivos texto ".txt"
pensando em performance, o método de findAll() alimenta uma lista em memória para as próximas
consultas serem performáticas.    

![picture](readme%20img/Package%20dao.png)
    

    


**Camada de regra de negócio Service**

A camada de regra de negócio da aplicação foi pensada em escalabilidade,
reusabilidade e fácil entendimento, sendo adotado um padão de interfaces
onde padroniza os métodos que o service irá utilizar para ficar fácil a troca
da referência de cada service.    

![picture](readme%20img/Package%20service.png)
    

    


**Exceptions da aplicação**

Exceptions personalizadas utilizadas na aplicação.    

![picture](readme%20img/Package%20exception.png)
    

    


**Classes UTIL da Aplicação**

O package UTIL acopla as classes que toda a aplicação utilizará.

Foi adotado o modelo de internacionalização da aplicação onde existe um
arquivo de mensagens, onde acopla todas as mensagens do sistema
separados pelo idioma;    

![picture](readme%20img/Package%20util.png)
    

    


**Visão geral do diagrama da aplicação**    

![picture](readme%20img/Package%20shopping.png)
    

    


**Testes**

Testes unitários da aplicação
![picture](readme%20img/Package%20tests.png)
    

    


**Arquivos de Configuração**

O sistema requer que seja informado um arquivo de configuração de nome config.properties.
Nele contém os parâmetros para aplicação localizar os arquivos de inputs e o caminho do arquivo de output.
É importante que seja criada uma pasta raiz para abrigar os arquivos de configuração, inputs e output.    


Exemplo: "C:/arquivos_shopping" onde essa pasta será a raiz e abrigará todos os outros arquivos. A pasta pode estar localizada em qualquer lugar do computador.

Essa pasta raiz será compartilhada com o container Dockeer que irá rodar a aplicação, com isso o arquivo gerado 
será facilmente obtido na máquina local.

**Arquivo de configuração para rodar a aplicação via Docker**
~~~~
#Modelo do arquivo de configuração
#Onde "/tmp/arquivos_shopping/" é a pasta dentro do docker que recebe os arquivos de configuração quando o container é iniciado, portanto, não mexa.
#Crie um arquivo de texto com o nome config.properties, copie e cole as configurações abaixo e altere os NOMES dos arquivos para os que seão utilizados.

#Caminho dos arquivos de inputs
productList=/tmp/arquivos_shopping/product.txt
discountList=/tmp/arquivos_shopping/discount.txt
purchaseOrderList=/tmp/arquivos_shopping/purchaseOrder.txt

#Caminho do arquivo de saida
invoicesOutput=/tmp/arquivos_shopping/
~~~~

**Arquivo de configuração para rodar a aplicação na máquina local**
~~~~
#Rodando a aplicação localmente
#Modelo do arquivo de configuração
#Onde "C:/arquivos_shopping" pode ser substituído por qualquer outro local de sua preferência
#Crie um arquivo de texto com o nome config.properties, copie e cole as configurações abaixo e altere o caminho da pasta raiz como desejar.

#Caminho dos arquivos de inputs
productList=C:/arquivos_shopping/product.txt
discountList=C:/arquivos_shopping/discount.txt
purchaseOrderList=C:/arquivos_shopping/purchaseOrder.txt

#Caminho do arquivo de saida
invoicesOutput=C:/arquivos_shopping
~~~~


**Rodando a aplicação com Docker**

Para realizar essas operações é necessário ter instalado o [Docker](https://www.docker.com/products/docker-desktop).
A aplicação irá rodar dentro de um container docker com Java e Maven,     
Ao buildar a imagem do container o projeto maven é injetado na imagem e é gerado o arquivo .JAR do projeto.    


Os passos para criar a imagem e rodar o container do docker são:    

1. Entrar na pasta raiz do projeto "desafio-022021-ricardophillipe/"    
2. Abrir o terminal ou o cmd na pasta do projeto    
3. Rodar os comando em sequência como é mostrado a seguir no exemplo    


~~~~
#1. - Criar a imagem docker a partir do Dockerfile dentro da pasta do projeto

docker build -t desafio-022021-ricardophillipe .

#2. - Rodar um container com a imagem criada.
#A opção --rm serve para excluir o container quando a aplicação finalizar
#A opção -v cria uma pasta compartilhada a partir de uma pasta da maquina local, então substitua "E:/arquivos shopping/" pela pasta onde está os arquivos da aplicaçãoe o arquivo da configuração

docker run -it --rm --name desafio-022021-ricardophillipe -v "E:/arquivos shopping/":/tmp/arquivos_shopping/ desafio-022021-ricardophillipe
~~~~



**Rodando a aplicação no computador local por linha de comando**

É necessário gerar o .JAR para rodar a aplicação, este procedimento é feito por via do [Maven](https://maven.apache.org/download.cgi).
Tendo o [Java JDK 8](https://www.oracle.com/br/java/technologies/javase/javase-jdk8-downloads.html) e o [Maven](https://maven.apache.org/download.cgi) instalados na máquina,
entre na pasta raiz do projeto abre o terminal ou CMD e rode o seguinte comando:

~~~~
mvn package
~~~~

Irá ser gerado o seguinte arquivo na pasta target: "./target/desafio-022021-RicardoPhillipe.jar".

Para rodar a aplicação, precisamos passar um parâmetro antes de rodar o .JAR informando
onde está o arquivo config.properties que contém as configurações dos arquivos de texto que a
aplicação precisa para rodar, o mesmo mostrado na sessão de configuração acima.

~~~~
#Entre na pasta target/ que fica licalizada na raiz do projeto, ela é criada após buildar o projeto e gerar o desafio-022021-RicardoPhillipe.jar .
#Informe o caminho onde está o arquivo de config.properties
#Se o arquivo config.properties estiver devidamente configurado como mostrado anteriormente tudo ocorrerá normalmente e será exibido no console.

java -jar -Dconfig={caminho onde está o config.properties}/config.properties desafio-022021-RicardoPhillipe.jar
~~~~


