package br.com.couto.shopping;


import br.com.couto.shopping.exception.ConfigException;
import br.com.couto.shopping.exception.DiscountPayTakeException;
import br.com.couto.shopping.exception.PurchaseOrderException;
import br.com.couto.shopping.model.service.InvoicesImplTextFileService;
import br.com.couto.shopping.model.service.InvoicesService;
import br.com.couto.shopping.util.Message;
import br.com.couto.shopping.util.ResourceUtil;
import br.com.couto.shopping.util.Show;

public class DesafioShoppingMain {

    public static void main(String[] args) {
        try {
            Show.printBanner();

            InvoicesService invoicesImplTextFileService = new InvoicesImplTextFileService();
            invoicesImplTextFileService.generateInvoices();

        } catch (ConfigException | PurchaseOrderException | DiscountPayTakeException e) {
            System.err.println(e.getMessage());
        } finally {
            System.out.println("\n" + ResourceUtil.msg(Message.APP_FINALIZED));
        }
    }

}
