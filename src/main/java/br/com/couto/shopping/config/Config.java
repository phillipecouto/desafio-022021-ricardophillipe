package br.com.couto.shopping.config;

import br.com.couto.shopping.exception.ConfigException;
import br.com.couto.shopping.util.Constants;
import br.com.couto.shopping.util.Message;
import br.com.couto.shopping.util.ResourceUtil;

import java.io.FileInputStream;
import java.io.IOException;
import java.util.Properties;

public class Config {

    private Properties properties;
    private static Config singleton;

    private Config() {
        loadConfig();
        validateConfig();
    }

    private void validateConfig() {
        String[] keys = new String[]{
                Constants.PURCHASE_ORDER_LIST,
                Constants.INVOICES_OUTPUT,
                Constants.DISCOUNT_LIST,
                Constants.PRODUCT_LIST};

        for (String key : keys) {
            if (properties.getProperty(key) == null) {
                throw new ConfigException(ResourceUtil.msg(Message.EXCEPTION_CONFIG_NOT_FOUND, key));
            }
        }
    }

    private void loadConfig() {
        if (System.getProperty(Constants.CONFIG) != null) {
            readerConfig();
            return;
        }
        properties = System.getProperties();
    }

    public void readerConfig() {
        try (FileInputStream fis = new FileInputStream(System.getProperty(Constants.CONFIG))) {
            properties = new Properties();
            properties.load(fis);
        } catch (IOException e) {
            throw new ConfigException(ResourceUtil.msg(Message.EXCEPTION_FILE_NOT_FOUND, Constants.CONFIG));
        }

    }

    public String getPath(String key) {
        if (properties.getProperty(key) == null) {
            throw new ConfigException(ResourceUtil.msg(Message.EXCEPTION_CONFIG_NOT_FOUND, key));
        }
        return properties.getProperty(key);
    }

    public static Config getSingleton() {
        if (singleton == null) {
            singleton = new Config();
        }
        return singleton;
    }
}
