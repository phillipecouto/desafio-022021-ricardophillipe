package br.com.couto.shopping.exception;

public class ConfigException extends RuntimeException {

    public ConfigException(String message) {
        super(message);
    }
}
