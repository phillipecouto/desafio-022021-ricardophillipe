package br.com.couto.shopping.exception;

public class DiscountPayTakeException extends RuntimeException {
    public DiscountPayTakeException(String message) {
        super(message);
    }
}
