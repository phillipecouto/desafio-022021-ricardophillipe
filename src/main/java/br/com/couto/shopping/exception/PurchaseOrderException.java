package br.com.couto.shopping.exception;

public class PurchaseOrderException extends RuntimeException {

    public PurchaseOrderException(String message) {
        super(message);
    }
}
