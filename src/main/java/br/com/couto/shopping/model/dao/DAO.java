package br.com.couto.shopping.model.dao;

import java.util.List;

public interface DAO<E> {

    List<E> findAll();

    E findByName(String name);

}
