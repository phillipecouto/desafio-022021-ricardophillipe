package br.com.couto.shopping.model.dao;


import br.com.couto.shopping.exception.DiscountPayTakeException;
import br.com.couto.shopping.model.entity.DiscountPayTake;
import br.com.couto.shopping.model.entity.Product;
import br.com.couto.shopping.util.Constants;
import br.com.couto.shopping.util.Message;
import br.com.couto.shopping.util.ProgressBarConsole;
import br.com.couto.shopping.util.ResourceUtil;

import java.io.BufferedReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class DiscountPayTakeImplTextFileDAO extends TextFileDAO implements DAO<DiscountPayTake> {

    private List<DiscountPayTake> discountPayTakeList;

    public DiscountPayTakeImplTextFileDAO() {
        super(Constants.DISCOUNT_LIST);
    }

    @Override
    public List<DiscountPayTake> findAll() {
        if (discountPayTakeList != null) {
            return discountPayTakeList;
        }

        BufferedReader file = getFile();
        String line = null;
        discountPayTakeList = new ArrayList<>();

        try {
            line = file.readLine();

            if (line == null) {
                return discountPayTakeList;
            }

            int count = Integer.parseInt(line);

            System.out.println(ResourceUtil.msg(Message.GENERATE_LIST, ResourceUtil.msg(Message.DISCOUNT)));

            for (int i = 0; i < count; i++) {
                ProgressBarConsole.progressPercentage(i + 1, count);
                DiscountPayTake d = DiscountPayTake.builder()
                        .product(Product.builder().name(file.readLine()).build())
                        .take(Integer.parseInt(file.readLine()))
                        .pay(Integer.parseInt(file.readLine()))
                        .build();
                discountPayTakeList.add(d);
            }

        } catch (Exception e) {
            throw new DiscountPayTakeException(ResourceUtil.msg(Message.EXCEPTION_DISCOUNT_PAY_TAKE) + ": " + e.getMessage());
        } finally {
            try {
                file.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

        return discountPayTakeList;
    }

    @Override
    public DiscountPayTake findByName(String name) {
        return null;
    }
}
