package br.com.couto.shopping.model.dao;


import br.com.couto.shopping.exception.PurchaseOrderException;
import br.com.couto.shopping.model.entity.Product;
import br.com.couto.shopping.util.Constants;
import br.com.couto.shopping.util.Message;
import br.com.couto.shopping.util.ProgressBarConsole;
import br.com.couto.shopping.util.ResourceUtil;

import java.io.BufferedReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class ProductImplTextFileDAO extends TextFileDAO implements DAO<Product> {

    public ProductImplTextFileDAO() {
        super(Constants.PRODUCT_LIST);
    }

    private List<Product> productList;

    @Override
    public List<Product> findAll() {

        if (productList != null) {
            return productList;
        }

        BufferedReader file = getFile();

        String line = null;
        productList = new ArrayList<>();

        try {
            line = file.readLine();

            if (line == null) {
                return productList;
            }

            int count = Integer.parseInt(line);

            System.out.println(ResourceUtil.msg(Message.GENERATE_LIST, ResourceUtil.msg(Message.PRODUCT)));

            for (int i = 0; i < count; i++) {
                ProgressBarConsole.progressPercentage(i + 1, count);
                Product p = Product.builder()
                        .name(file.readLine())
                        .price(Integer.parseInt(file.readLine()))
                        .build();
                productList.add(p);
            }

        } catch (Exception e) {
            throw new PurchaseOrderException(ResourceUtil.msg(Message.EXCEPTION_PRODUCT) + ": " + e.getMessage());
        } finally {
            try {
                file.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

        return productList;
    }

    @Override
    public Product findByName(String name) {
        for (Product p : findAll()) {
            if (p.getName().equals(name)) {
                return p;
            }
        }

        return null;
    }
}
