package br.com.couto.shopping.model.dao;


import br.com.couto.shopping.exception.PurchaseOrderException;
import br.com.couto.shopping.model.entity.Product;
import br.com.couto.shopping.model.entity.PurchaseOrder;
import br.com.couto.shopping.util.Constants;
import br.com.couto.shopping.util.Message;
import br.com.couto.shopping.util.ProgressBarConsole;
import br.com.couto.shopping.util.ResourceUtil;

import java.io.BufferedReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class PurchaseOrderImplTextFileDAO extends TextFileDAO implements DAO<PurchaseOrder> {

    List<PurchaseOrder> purchaseOrderList;

    public PurchaseOrderImplTextFileDAO() {
        super(Constants.PURCHASE_ORDER_LIST);
    }

    @Override
    public List<PurchaseOrder> findAll() {
        if (purchaseOrderList != null) {
            return purchaseOrderList;
        }

        BufferedReader file = getFile();
        String line = null;
        purchaseOrderList = new ArrayList<>();

        try {
            line = file.readLine();

            if (line == null) {
                throw new PurchaseOrderException(ResourceUtil.msg(Message.EXCEPTION_PURCHASE_ORDER_EMPTY));
            }

            int count = Integer.parseInt(line);

            System.out.println(ResourceUtil.msg(Message.GENERATE_LIST, ResourceUtil.msg(Message.PURCHASE)));

            for (int i = 0; i < count; i++) {
                ProgressBarConsole.progressPercentage(i + 1, count);
                PurchaseOrder p = PurchaseOrder.builder()
                        .id(Integer.parseInt(file.readLine()))
                        .amountProducts(Integer.parseInt(file.readLine()))
                        .productList(new ArrayList<>())
                        .build();

                for (int j = 0; j < p.getAmountProducts(); j++) {
                    Product prod = Product.builder()
                            .name(file.readLine())
                            .build();
                    p.getProductList().add(prod);
                }

                purchaseOrderList.add(p);
            }

        } catch (Exception e) {
            throw new PurchaseOrderException(ResourceUtil.msg(Message.EXCEPTION_PURCHASE_ORDER) + ": " + e.getMessage());
        } finally {
            try {
                file.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

        return purchaseOrderList;
    }

    @Override
    public PurchaseOrder findByName(String name) {
        return null;
    }
}
