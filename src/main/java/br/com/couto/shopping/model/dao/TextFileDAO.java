package br.com.couto.shopping.model.dao;


import br.com.couto.shopping.config.Config;
import br.com.couto.shopping.exception.ConfigException;
import br.com.couto.shopping.util.Message;
import br.com.couto.shopping.util.ResourceUtil;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;

public class TextFileDAO {

    private final String path;

    public TextFileDAO(String prop) {
        path = Config.getSingleton().getPath(prop);
    }

    public BufferedReader getFile() {
        FileReader arq;
        try {
            arq = new FileReader(path);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
            throw new ConfigException(ResourceUtil.msg(Message.EXCEPTION_FILE_NOT_FOUND, path));
        }
        return new BufferedReader(arq);
    }


}
