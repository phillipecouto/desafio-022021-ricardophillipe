package br.com.couto.shopping.model.entity;

public class DiscountPayTake {
    private Product product;
    private int take;
    private int pay;

    public DiscountPayTake(Builder builder) {
        this.product = builder.product;
        this.take = builder.take;
        this.pay = builder.pay;
    }

    public static DiscountPayTake.Builder builder() {
        return new DiscountPayTake.Builder();
    }

    public Product getProduct() {
        return product;
    }

    public void setProduct(Product product) {
        this.product = product;
    }

    public int getTake() {
        return take;
    }

    public void setTake(int take) {
        this.take = take;
    }

    public int getPay() {
        return pay;
    }

    public void setPay(int pay) {
        this.pay = pay;
    }

    public static class Builder {
        private Product product;
        private int take;
        private int pay;

        public Builder() {
            //Builder Pattern
        }

        public Builder product(Product product) {
            this.product = product;
            return this;
        }

        public Builder take(int take) {
            this.take = take;
            return this;
        }

        public Builder pay(int pay) {
            this.pay = pay;
            return this;
        }

        public DiscountPayTake build() {
            return new DiscountPayTake(this);
        }
    }

    @Override
    public String toString() {
        return "DiscountPayTake{" +
                "product=" + product +
                ", take=" + take +
                ", pay=" + pay +
                '}';
    }


}
