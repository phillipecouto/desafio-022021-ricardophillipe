package br.com.couto.shopping.model.entity;

public class Invoices {
    private int id;
    private int amountItens;
    private int totalInvoice;

    public Invoices(Builder builder) {
        this.id = builder.id;
        this.amountItens = builder.amountItens;
        this.totalInvoice = builder.totalInvoice;
    }

    public static Invoices.Builder builder() {
        return new Invoices.Builder();
    }

    public static class Builder {
        private int id;
        private int amountItens;
        private int totalInvoice;

        public Builder() {
            //Builder Pattern
        }

        public Builder id(int id) {
            this.id = id;
            return this;
        }

        public Builder amountItens(int amountItens) {
            this.amountItens = amountItens;
            return this;
        }

        public Builder totalInvoice(int totalInvoice) {
            this.totalInvoice = totalInvoice;
            return this;
        }

        public Invoices build() {
            return new Invoices(this);
        }

    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getAmountItens() {
        return amountItens;
    }

    public void setAmountItens(int amountItens) {
        this.amountItens = amountItens;
    }

    public int getTotalInvoice() {
        return totalInvoice;
    }

    public void setTotalInvoice(int totalInvoice) {
        this.totalInvoice = totalInvoice;
    }

    @Override
    public String toString() {
        return "Invoices{" +
                "id=" + id +
                ", amountItens=" + amountItens +
                ", totalInvoice=" + totalInvoice +
                '}';
    }
}
