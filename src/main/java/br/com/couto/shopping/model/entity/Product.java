package br.com.couto.shopping.model.entity;

public class Product {
    private String name;
    private int price;
    private int quantity;

    private Product(Builder builder) {
        this.name = builder.name;
        this.price = builder.price;
        this.quantity = builder.quantity;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getPrice() {
        return price;
    }

    public void setPrice(int price) {
        this.price = price;
    }

    public int getQuantity() {
        return quantity;
    }

    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }

    public static Product.Builder builder() {
        return new Product.Builder();
    }

    public static class Builder {
        private String name;
        private int price;
        private int quantity;

        public Builder() {
            //Builder Pattern
        }

        public Builder name(String name) {
            this.name = name;
            return this;
        }

        public Builder price(int price) {
            this.price = price;
            return this;
        }

        public Builder quantity(int quantity) {
            this.quantity = quantity;
            return this;
        }

        public Product build() {
            return new Product(this);
        }

    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) return false;
        if (this.name.equals(((Product) obj).getName())) return true;
        return super.equals(obj);
    }

    @Override
    public String toString() {
        return "Product{" +
                "name='" + name + '\'' +
                ", price=" + price +
                ", quantity=" + quantity +
                '}';
    }
}
