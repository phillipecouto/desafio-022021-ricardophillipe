package br.com.couto.shopping.model.entity;

import java.util.List;

public class PurchaseOrder implements Comparable<PurchaseOrder> {

    private int id;
    private int amountProducts;
    private int discountValue;
    private int productsValue;
    private List<Product> productList;

    public PurchaseOrder(Builder builder) {
        this.id = builder.id;
        this.amountProducts = builder.amountProducts;
        this.discountValue = builder.discountValue;
        this.productsValue = builder.productsValue;
        this.productList = builder.productList;
    }

    public static PurchaseOrder.Builder builder() {
        return new PurchaseOrder.Builder();
    }

    public static class Builder {
        private int id;
        private int amountProducts;
        private int discountValue;
        private int productsValue;
        private List<Product> productList;

        public Builder() {
            //Builder Pattern
        }

        public Builder id(int id) {
            this.id = id;
            return this;
        }

        public Builder amountProducts(int amountProducts) {
            this.amountProducts = amountProducts;
            return this;
        }

        public Builder discountValue(int discountValue) {
            this.discountValue = discountValue;
            return this;
        }

        public Builder productsValue(int productsValue) {
            this.productsValue = productsValue;
            return this;
        }

        public Builder productList(List<Product> productList) {
            this.productList = productList;
            return this;
        }

        public PurchaseOrder build() {
            return new PurchaseOrder(this);
        }
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getAmountProducts() {
        return amountProducts;
    }

    public void setAmountProducts(int amountProducts) {
        this.amountProducts = amountProducts;
    }

    public int getDiscountValue() {
        return discountValue;
    }

    public void setDiscountValue(int discountValue) {
        this.discountValue = discountValue;
    }

    public int getProductsValue() {
        return productsValue;
    }

    public void setProductsValue(int productsValue) {
        this.productsValue = productsValue;
    }

    public List<Product> getProductList() {
        return productList;
    }

    public void setProductList(List<Product> productList) {
        this.productList = productList;
    }

    @Override
    public String toString() {
        return "PurchaseOrder{" +
                "id=" + id +
                ", amountProducts=" + amountProducts +
                ", discountValue=" + discountValue +
                ", productsValue=" + productsValue +
                ", productList=" + productList +
                '}';
    }

    @Override
    public int compareTo(PurchaseOrder o) {
        if (this.id > o.id) return 1;
        if (this.id < o.id) return -1;
        return 0;
    }
}
