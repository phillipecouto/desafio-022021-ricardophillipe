package br.com.couto.shopping.model.service;


import br.com.couto.shopping.model.dao.DAO;
import br.com.couto.shopping.model.dao.DiscountPayTakeImplTextFileDAO;
import br.com.couto.shopping.model.entity.DiscountPayTake;
import br.com.couto.shopping.model.entity.Product;
import br.com.couto.shopping.model.entity.PurchaseOrder;
import br.com.couto.shopping.util.Message;
import br.com.couto.shopping.util.ProgressBarConsole;
import br.com.couto.shopping.util.ResourceUtil;

import java.util.List;

public class DiscountPayTakeImplTextFileService implements DiscountPayTakeService {

    private DAO<DiscountPayTake> dao;

    public DiscountPayTakeImplTextFileService() {
        dao = new DiscountPayTakeImplTextFileDAO();
    }

    @Override
    public List<DiscountPayTake> findAll() {
        return dao.findAll();
    }

    private DiscountPayTake getDiscountByProductNameInList(List<DiscountPayTake> discountPayTakeList, String productName) {
        for (DiscountPayTake dis : discountPayTakeList) {
            if (dis.getProduct().getName().equals(productName)) return dis;
        }

        return null;
    }

    @Override
    public void applyDiscount(List<PurchaseOrder> purchaseOrderList) {
        List<DiscountPayTake> discountPayTakeList = findAll();

        if (discountPayTakeList == null || discountPayTakeList.isEmpty()) {
            return;
        }

        System.out.println(ResourceUtil.msg(Message.DISCOUNT_APPLY));

        int count = 1;

        for (PurchaseOrder purchase : purchaseOrderList) {
            ProgressBarConsole.progressPercentage(count++, purchaseOrderList.size());
            discountValidAndCalculate(discountPayTakeList, purchase);
        }
    }

    private void discountValidAndCalculate(List<DiscountPayTake> discountPayTakeList, PurchaseOrder purchase) {
        int productValueWithDiscount;
        int productsValue;
        int discountValue;
        DiscountPayTake discountPayTake;

        for (Product product : purchase.getProductList()) {
            discountPayTake = getDiscountByProductNameInList(discountPayTakeList, product.getName());
            if (discountPayTake != null && product.getQuantity() >= discountPayTake.getTake()) {
                productsValue = (product.getPrice() * product.getQuantity());
                productValueWithDiscount = ((((product.getQuantity()) / discountPayTake.getTake()) * discountPayTake.getPay()) * product.getPrice());
                discountValue = productsValue - productValueWithDiscount;

                purchase.setProductsValue(purchase.getProductsValue() - productsValue);
                purchase.setProductsValue(purchase.getProductsValue() + productValueWithDiscount);
                purchase.setDiscountValue(discountValue);
            }
        }
    }

}
