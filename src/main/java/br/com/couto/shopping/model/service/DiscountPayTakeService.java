package br.com.couto.shopping.model.service;

import br.com.couto.shopping.model.entity.DiscountPayTake;
import br.com.couto.shopping.model.entity.PurchaseOrder;

import java.util.List;

public interface DiscountPayTakeService {

    List<DiscountPayTake> findAll();

    void applyDiscount(List<PurchaseOrder> purchaseOrderList);

}
