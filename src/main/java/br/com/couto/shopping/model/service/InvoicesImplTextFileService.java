package br.com.couto.shopping.model.service;


import br.com.couto.shopping.config.Config;
import br.com.couto.shopping.model.entity.Invoices;
import br.com.couto.shopping.model.entity.PurchaseOrder;
import br.com.couto.shopping.util.Constants;
import br.com.couto.shopping.util.Message;
import br.com.couto.shopping.util.ProgressBarConsole;
import br.com.couto.shopping.util.ResourceUtil;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class InvoicesImplTextFileService implements InvoicesService {

    private final PurchaseOrderService purchaseOrderImplTextFileService;

    public InvoicesImplTextFileService() {
        purchaseOrderImplTextFileService = new PurchaseOrderImplTextFileOrderService();
    }

    private List<Invoices> getInvoicesList() {
        List<PurchaseOrder> purchaseOrderList = purchaseOrderImplTextFileService.findAll();
        Collections.sort(purchaseOrderList);
        List<Invoices> invoicesList = new ArrayList<>();
        int count = 1;

        if (purchaseOrderList.size() > 0) {
            System.out.println(ResourceUtil.msg(Message.GENERATE_LIST, ResourceUtil.msg(Message.INVOICE)));
        }

        for (PurchaseOrder purhcase : purchaseOrderList) {
            ProgressBarConsole.progressPercentage(count++, purchaseOrderList.size());
            Invoices invoices = Invoices.builder()
                    .id(purhcase.getId())
                    .amountItens(purhcase.getAmountProducts())
                    .totalInvoice(purhcase.getProductsValue())
                    .build();
            invoicesList.add(invoices);

        }

        return invoicesList;
    }

    private void generateInvoicesFile(List<Invoices> invoicesList) {
        Config propertyValues = Config.getSingleton();
        BufferedWriter buffWrite;
        int count = 1;
        try {
            buffWrite = new BufferedWriter(new FileWriter(propertyValues.getPath(Constants.INVOICES_OUTPUT) + "/invoices.txt"));
            try {
                buffWrite.append(String.valueOf(invoicesList.size())).append("\n");

                if (invoicesList.size() > 0) {
                    System.out.println(ResourceUtil.msg(Message.WRITE_INVOICES));
                }

                for (Invoices invoices : invoicesList) {
                    ProgressBarConsole.progressPercentage(count++, invoicesList.size());

                    buffWrite.append(String.valueOf(invoices.getId())).append("\n");
                    buffWrite.append(String.valueOf(invoices.getAmountItens())).append("\n");
                    buffWrite.append(String.valueOf(invoices.getTotalInvoice())).append("\n");
                }
            } finally {
                buffWrite.close();
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void generateInvoices() {
        generateInvoicesFile(getInvoicesList());
    }

}
