package br.com.couto.shopping.model.service;


import br.com.couto.shopping.model.dao.DAO;
import br.com.couto.shopping.model.dao.ProductImplTextFileDAO;
import br.com.couto.shopping.model.entity.Product;

import java.util.List;

public class ProductImplTextFileService implements ProductService {

    private DAO<Product> dao;

    public ProductImplTextFileService() {
        dao = new ProductImplTextFileDAO();
    }

    @Override
    public List<Product> findAll() {
        return dao.findAll();
    }

    @Override
    public Product findByName(String name) {
        return dao.findByName(name);
    }

}
