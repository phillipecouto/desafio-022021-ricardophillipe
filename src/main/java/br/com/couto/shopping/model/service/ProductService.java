package br.com.couto.shopping.model.service;

import br.com.couto.shopping.model.entity.Product;

import java.util.List;

public interface ProductService {

    List<Product> findAll();

    Product findByName(String name);

}
