package br.com.couto.shopping.model.service;


import br.com.couto.shopping.exception.PurchaseOrderException;
import br.com.couto.shopping.model.dao.DAO;
import br.com.couto.shopping.model.dao.PurchaseOrderImplTextFileDAO;
import br.com.couto.shopping.model.entity.Product;
import br.com.couto.shopping.model.entity.PurchaseOrder;
import br.com.couto.shopping.util.Message;
import br.com.couto.shopping.util.ResourceUtil;

import java.util.ArrayList;
import java.util.List;

public class PurchaseOrderImplTextFileOrderService implements PurchaseOrderService {
    private final DAO<PurchaseOrder> dao;
    private final ProductService productImplTextFileService;
    private final DiscountPayTakeService discountPayTakeImplTextFileService;


    public PurchaseOrderImplTextFileOrderService() {
        dao = new PurchaseOrderImplTextFileDAO();
        productImplTextFileService = new ProductImplTextFileService();
        discountPayTakeImplTextFileService = new DiscountPayTakeImplTextFileService();
    }

    @Override
    public List<PurchaseOrder> findAll() {
        List<PurchaseOrder> purchaseOrderList = dao.findAll();
        List<Product> productList;

        for (PurchaseOrder purchase : purchaseOrderList) {
            productList = new ArrayList<>();
            setProducts(productList, purchase);
            purchase.setProductList(productList);
        }

        discountPayTakeImplTextFileService.applyDiscount(purchaseOrderList);
        return purchaseOrderList;
    }

    private void setProducts(List<Product> productList, PurchaseOrder purchase) {
        for (Product product : purchase.getProductList()) {
            Product prod = productImplTextFileService.findByName(product.getName());

            if (prod == null) {
                throw new PurchaseOrderException(ResourceUtil.msg(Message.EXCEPTION_PURCHASE_ORDER_PRODUCT_NOTFOUND, product.getName(), String.valueOf(purchase.getId())));
            }

            product.setPrice(prod.getPrice());
            product.setQuantity(1);
            purchase.setProductsValue(purchase.getProductsValue() + product.getPrice());

            if (productList.contains(product)) {
                productList.get(productList.indexOf(product)).setPrice(product.getPrice());
                productList.get(productList.indexOf(product)).setQuantity(productList.get(productList.indexOf(product)).getQuantity() + 1);
            } else {
                productList.add(product);
            }
        }
    }

}
