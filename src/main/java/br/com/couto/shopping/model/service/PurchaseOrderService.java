package br.com.couto.shopping.model.service;

import br.com.couto.shopping.model.entity.PurchaseOrder;

import java.util.List;

public interface PurchaseOrderService {

    List<PurchaseOrder> findAll();


}
