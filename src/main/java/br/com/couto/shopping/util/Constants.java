package br.com.couto.shopping.util;

public class Constants {
    public static final String CONFIG = "config";
    public static final String PRODUCT_LIST = "productList";
    public static final String DISCOUNT_LIST = "discountList";
    public static final String PURCHASE_ORDER_LIST = "purchaseOrderList";
    public static final String INVOICES_OUTPUT = "invoicesOutput";
}
