package br.com.couto.shopping.util;

public class Message {
    public static final String DISCOUNT_APPLY = "discount_applay";
    public static final String GENERATE_LIST = "generate_list";
    public static final String DISCOUNT = "discount";
    public static final String INVOICE = "invoice";
    public static final String PRODUCT = "product";
    public static final String PURCHASE = "purchase";
    public static final String WRITE_INVOICES = "write_invoices";
    public static final String APP_FINALIZED = "app_finalized";

    //Exceptions
    public static final String EXCEPTION_CONFIG_NOT_FOUND = "exception_config_not_found";
    public static final String EXCEPTION_FILE_NOT_FOUND = "exception_file_not_found";
    public static final String EXCEPTION_PURCHASE_ORDER = "exception_purchase_order";
    public static final String EXCEPTION_PURCHASE_ORDER_EMPTY = "exception_purchase_order_empty";
    public static final String EXCEPTION_PURCHASE_ORDER_PRODUCT_NOTFOUND = "exception_purchase_order_product_notfound";
    public static final String EXCEPTION_DISCOUNT_PAY_TAKE = "exception_discount_pay_take";
    public static final String EXCEPTION_PRODUCT = "exception_product";

}
