package br.com.couto.shopping.util;

import java.text.MessageFormat;
import java.util.Locale;
import java.util.ResourceBundle;

public class ResourceUtil {

    private static final ResourceBundle bundle;

    static {
        bundle = ResourceBundle.getBundle("messages_pt_BR", new Locale("pt_br"));
    }


    public static String msg(String key) {
        return bundle.getString(key);
    }

    public static String msg(String key, String... values) {
        return MessageFormat.format(msg(key), values);
    }

    public static String getVersion() {
        final ResourceBundle bundle = ResourceBundle.getBundle("build");
        return bundle.getString("version");
    }
}