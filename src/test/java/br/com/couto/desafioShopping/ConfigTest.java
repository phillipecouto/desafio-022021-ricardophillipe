package br.com.couto.desafioShopping;

import br.com.couto.shopping.config.Config;
import br.com.couto.shopping.exception.ConfigException;
import org.junit.Test;

public class ConfigTest {

    @Test(expected = ConfigException.class)
    public void fileProductListNotFound() {
        Config.getSingleton();
    }

}
