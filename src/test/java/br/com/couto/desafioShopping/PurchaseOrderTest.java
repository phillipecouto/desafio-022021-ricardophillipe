package br.com.couto.desafioShopping;

import br.com.couto.shopping.exception.PurchaseOrderException;
import br.com.couto.shopping.model.service.PurchaseOrderImplTextFileOrderService;
import br.com.couto.shopping.model.service.PurchaseOrderService;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;

public class PurchaseOrderTest {

    private PurchaseOrderService purchaseOrderImplTextFileService;
    File fileProductList;
    File fileDiscountList;
    File filePurchaseOrderList;

    @Before
    public void setUp() {

        try {
            createTempFiles();

            setSystemProperties();

            writePurchaseOrderTempFile();

        } catch (IOException e) {
            e.printStackTrace();
        }

        purchaseOrderImplTextFileService = new PurchaseOrderImplTextFileOrderService();

    }

    @After
    public void tearDown() {
        fileProductList.delete();
        fileDiscountList.delete();
        filePurchaseOrderList.delete();
    }

    @Test(expected = PurchaseOrderException.class)
    public void discountWithProductNotFoundTest() {
        purchaseOrderImplTextFileService.findAll();
    }

    private void createTempFiles() throws IOException {
        fileProductList = File.createTempFile("product", ".txt");
        fileDiscountList = File.createTempFile("discount", ".txt");
        filePurchaseOrderList = File.createTempFile("purchase", ".txt");
    }

    private void writePurchaseOrderTempFile() throws IOException {
        BufferedWriter buffWrite = new BufferedWriter(new FileWriter(filePurchaseOrderList.getAbsolutePath()));
        try {
            buffWrite.append("1\n");
            buffWrite.append("1\n");
            buffWrite.append("1\n");
            buffWrite.append("Milk\n");
        } finally {
            buffWrite.close();
        }
    }

    private void setSystemProperties() {
        System.setProperty("productList", fileProductList.getAbsolutePath());
        System.setProperty("discountList", fileDiscountList.getAbsolutePath());
        System.setProperty("purchaseOrderList", filePurchaseOrderList.getAbsolutePath());
        System.setProperty("invoicesOutput", filePurchaseOrderList.getPath());
    }
}
